package colourVision;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;

public class AverageColour {

    private int backgroundRangeToIgnore = 50;
    private double boundaryRange = 1;
    private int lowerRed = 0;
    private int upperRed = 0;
    private int lowerGreen = 0;
    private int upperGreen = 0;
    private int lowerBlue = 0;
    private int upperBlue = 0;
    private int totalArea = 0;
    private Color backgroundColour;
    private ArrayList<ObjectContainer> objects = new ArrayList<>();
    private ArrayList<Color> colours = new ArrayList<>();

    public Color[] getColoursEncoded(String path) {
        BufferedImage image = loadImage(path);
        int y = 0;
        do {
            y = processTrack(image, y);
        }
        while(y < image.getHeight());
        double averageSize = totalArea/objects.size();
        for (ObjectContainer object : objects) {
            if(object.getSquareArea()>0.3*averageSize) {
                colours.add(object.getColour());
            }
        }
        return GeneralizeColours.process(colours);
    }

    private int processTrack(BufferedImage image, int startY) {
        int lowerQuartile = 0;
        int upperQuartile = 0;
        boolean foundObject = false;
        outerLoop:
        for (int y = startY; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                Color pixel = new Color(image.getRGB(x,y));
                if(pixelValid(pixel)) {
                    ObjectContainer object = floodSearch(cloneImage(image),x,y,pixel);
                    int center = object.getCenterOfMassY();
                    upperQuartile = (int)(center + (object.getMaxY()-center)/boundaryRange+1);
                    lowerQuartile = (int)(center - (center-object.getMinY())/boundaryRange+1);
                    if(center >= startY) {
                        foundObject = true;
                        break outerLoop;
                    }
                }
            }
        }
        if(foundObject) {
            ArrayList<ObjectContainer> track = new ArrayList<>();
            for (int x = 0; x < image.getWidth(); x++) {
                for (int y = lowerQuartile; y < upperQuartile; y++) {
                    Color pixel = new Color(image.getRGB(x, y));
                    if(pixelValid(pixel)) {
                        ObjectContainer object = floodSearch(image,x,y,pixel);
                        int center = object.getCenterOfMassY();
                        if(center >= lowerQuartile && center <= upperQuartile) {
                            track.add(object);
                            int newUpperQuartile = (int)(center + (object.getMaxY()-center)/boundaryRange+1);
                            int newLowerQuartile = (int)(center - (center-object.getMinY())/boundaryRange+1);
                            if (newLowerQuartile < lowerQuartile) {
                                lowerQuartile = newLowerQuartile;
                                for (ObjectContainer objectContainer : track) {
                                    image = redrawColour(image, objectContainer);
                                }
                                track = new ArrayList<>();
                                x = 0;
                            }
                            if (newUpperQuartile > upperQuartile) {
                                upperQuartile = newUpperQuartile;
                                for (ObjectContainer objectContainer : track) {
                                    image = redrawColour(image, objectContainer);
                                }
                                track = new ArrayList<>();
                                x = 0;
                            }
                            y = lowerQuartile;
                        } else {
                            image = redrawColour(image, object);
                        }
                    }
                }
            }
            objects.addAll(track);
            for (ObjectContainer container : track) {
                totalArea+=container.getSquareArea();
            }
            return upperQuartile+1;
        } else {
            return image.getHeight();
        }
    }

    private BufferedImage redrawColour(BufferedImage image, ObjectContainer object) {
        for(int xToDraw = object.getMinX(); xToDraw<= object.getMaxX();xToDraw++) {
            image.setRGB(xToDraw,object.getCenterOfMassY(),object.getColour().getRGB());
        }
        for(int yToDraw = object.getMinY(); yToDraw<= object.getMaxY();yToDraw++) {
            image.setRGB(object.getCenterOfMassX(),yToDraw,object.getColour().getRGB());
        }
        return image;
    }


    private boolean pixelValid(Color pixel) {
        boolean redValid = true;
        boolean greenValid = true;
        boolean blueValid = true;
        if(pixel.getRed() >= lowerRed && pixel.getRed() <= upperRed) {
            redValid = false;
        }
        if(pixel.getGreen() >= lowerGreen && pixel.getGreen() <= upperGreen) {
            greenValid = false;
        }
        if(pixel.getBlue() >= lowerBlue && pixel.getBlue() <= upperBlue) {
            blueValid = false;
        }
        return redValid || greenValid || blueValid;
    }

    private boolean colourSimilar(Color pixel1, Color pixel2) {
        float[] hsv1 = Color.RGBtoHSB(pixel1.getRed(), pixel1.getGreen(), pixel1.getBlue(), null);
        float[] hsv2 = Color.RGBtoHSB(pixel2.getRed(), pixel2.getGreen(), pixel2.getBlue(), null);
        double distance = Math.sqrt(Math.pow(hsv1[0] - hsv2[0], 2));
        double distance2 = Math.sqrt(Math.pow(hsv2[0]-hsv1[0],2));
        return Math.min(distance,distance2) <= 10;
    }

    private ObjectContainer floodSearch(BufferedImage image, int x, int y, Color colourToFind) {
        ObjectContainer object = new ObjectContainer();
        ArrayDeque<Point2D.Double> nodeQueue = new ArrayDeque<>();
        nodeQueue.add(new Point2D.Double(x, y));
        int nodeQueueSize = nodeQueue.size();
        for (int i = 0; i < nodeQueueSize; i++) {
            Point2D.Double node = nodeQueue.remove();
            Point2D.Double w = (Point2D.Double) node.clone();
            Point2D.Double e = (Point2D.Double) node.clone();
            do {
                w.setLocation(w.getX() - 1, w.getY());
                if (w.getX() < 0) {
                    break;
                }
            }
            while (pixelValid(new Color(image.getRGB((int) w.getX(), (int) w.getY()))) && colourSimilar(colourToFind, new Color(image.getRGB((int) w.getX(), (int) w.getY()))));
            do {
                e.setLocation(e.getX() + 1, e.getY());
                if (e.getX() >= image.getWidth()) {
                    break;
                }
            }
            while (pixelValid(new Color(image.getRGB((int) e.getX(), (int) e.getY()))) && colourSimilar(colourToFind, new Color(image.getRGB((int) e.getX(), (int) e.getY()))));
            for (int currentX = (int) w.getX() + 1; currentX <= e.getX() - 1; currentX++) {
                if (pixelValid(new Color(image.getRGB(currentX, (int) e.getY()))) && colourSimilar(colourToFind, new Color(image.getRGB(currentX, (int) e.getY())))) {
                    object.addPoint(new Point2D.Double(currentX, e.getY()), new Color(image.getRGB(currentX, (int) e.getY())));
                    image.setRGB(currentX, (int) e.getY(), backgroundColour.getRGB());
                    if (e.getY() != 0) {
                        if (pixelValid(new Color(image.getRGB(currentX, (int) e.getY() - 1))) && colourSimilar(colourToFind, new Color(image.getRGB(currentX, (int) e.getY() - 1)))) {
                            nodeQueue.add(new Point2D.Double(currentX, e.getY() - 1));
                            nodeQueueSize++;
                        }
                    }
                    if (e.getY() != image.getHeight()-1) {
                        if (pixelValid(new Color(image.getRGB(currentX, (int) e.getY() + 1))) && colourSimilar(colourToFind, new Color(image.getRGB(currentX, (int) e.getY() + 1)))) {
                            nodeQueue.add(new Point2D.Double(currentX, e.getY() + 1));
                            nodeQueueSize++;
                        }
                    }
                }
            }
        }
        return object;
    }

    public void setBackgroundColour(String path) {
        BufferedImage image = loadImage(path);
        long red = 0;
        long green = 0;
        long blue = 0;
        long pixelCount = 0;
        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                Color pixelColor = new Color(image.getRGB(x, y));
                red += pixelColor.getRed();
                if(pixelColor.getRed()<0) {
                    System.out.println(x);
                    System.out.println(y);
                }
                green += pixelColor.getGreen();
                if(pixelColor.getGreen()<0) {
                    System.out.println(x);
                    System.out.println(y);
                }
                blue += pixelColor.getBlue();
                if(pixelColor.getBlue()<0) {
                    System.out.println(x);
                    System.out.println(y);
                }
                pixelCount++;
            }
        }
        backgroundColour = new Color(Math.toIntExact(red/pixelCount),Math.toIntExact(green/pixelCount),Math.toIntExact(blue/pixelCount));
        setRanges(red, green, blue, pixelCount);
    }

    private void setRanges(long red, long green, long blue, long pixelCount) {
        lowerRed = adjustToRGBScale(Math.toIntExact(red/pixelCount)-backgroundRangeToIgnore);
        upperRed = adjustToRGBScale(Math.toIntExact(red/pixelCount)+backgroundRangeToIgnore);
        lowerGreen = adjustToRGBScale(Math.toIntExact(green/pixelCount)-backgroundRangeToIgnore);
        upperGreen = adjustToRGBScale(Math.toIntExact(green/pixelCount)+backgroundRangeToIgnore);
        lowerBlue = adjustToRGBScale(Math.toIntExact(blue/pixelCount)-backgroundRangeToIgnore);
        upperBlue = adjustToRGBScale(Math.toIntExact(blue/pixelCount)+backgroundRangeToIgnore);
    }

    private int adjustToRGBScale(int toAdjust) {
        if (toAdjust < 0) {
            return 0;
        } else if (toAdjust > 255) {
            return 255;
        } else {
            return toAdjust;
        }
    }

    private BufferedImage loadImage(String path) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(getClass().getResource("/resources/images/" + path));
        } catch (IOException e) {
            //todo make a better exception handler
            System.out.println(e.getMessage());
        }
        return image;
    }

    private BufferedImage cloneImage(BufferedImage image) {
        ColorModel cm = image.getColorModel();
        boolean isAlpha = cm.isAlphaPremultiplied();
        WritableRaster raster= image.copyData(null);
        return new BufferedImage(cm,raster,isAlpha,null);
    }

}
