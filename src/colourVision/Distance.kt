package colourVision

import java.awt.Color

object Distance {

    fun rgb2lab(colourToConvert: Color): DoubleArray {
        val R = colourToConvert.red
        val G = colourToConvert.green
        val B = colourToConvert.blue
        //http://www.brucelindbloom.com

        var r: Float
        var g: Float
        var b: Float
        val X: Float
        val Y: Float
        val Z: Float
        val fx: Float
        val fy: Float
        val fz: Float
        val xr: Float
        val yr: Float
        val zr: Float
        val Ls: Float
        val `as`: Float
        val bs: Float
        val eps = 216f / 24389f
        val k = 24389f / 27f

        val Xr = 0.96422f  // reference white D50
        val Yr = 1.0f
        val Zr = 0.82421f

        // RGB to XYZ
        r = R / 255f //R 0..1
        g = G / 255f //G 0..1
        b = B / 255f //B 0..1

        // assuming sRGB (D65)
        r = if (r <= 0.04045)
            r / 12
        else
            Math.pow((r + 0.055) / 1.055, 2.4).toFloat()

        g = if (g <= 0.04045)
            g / 12
        else
            Math.pow((g + 0.055) / 1.055, 2.4).toFloat()

        b = if (b <= 0.04045)
            b / 12
        else
            Math.pow((b + 0.055) / 1.055, 2.4).toFloat()


        X = 0.436052025f * r + 0.385081593f * g + 0.143087414f * b
        Y = 0.222491598f * r + 0.71688606f * g + 0.060621486f * b
        Z = 0.013929122f * r + 0.097097002f * g + 0.71418547f * b

        // XYZ to Lab
        xr = X / Xr
        yr = Y / Yr
        zr = Z / Zr

        fx = if (xr > eps)
            Math.pow(xr.toDouble(), 1 / 3.0).toFloat()
        else
            ((k * xr + 16.0) / 116.0).toFloat()

        fy = if (yr > eps)
            Math.pow(yr.toDouble(), 1 / 3.0).toFloat()
        else
            ((k * yr + 16.0) / 116.0).toFloat()

        fz = if (zr > eps)
            Math.pow(zr.toDouble(), 1 / 3.0).toFloat()
        else
            ((k * zr + 16.0) / 116).toFloat()

        //looks like correct lab
        Ls = 116 * fy - 16
        `as` = 500 * (fx - fy)
        bs = 200 * (fy - fz)

        return doubleArrayOf(Ls.toDouble(), `as`.toDouble(), bs.toDouble())
    }

    fun getDistance(colour: DoubleArray, colour2: DoubleArray): Double {
        val L1 = colour[0]
        val a1 = colour[1]
        val b1 = colour[2]
        val L2 = colour2[0]
        val a2 = colour2[1]
        val b2 = colour2[2]

        val parametric = 1.0

        val C1ab = Math.sqrt(Math.pow(a1, 2.0) + Math.pow(b1, 2.0))
        val C2ab = Math.sqrt(Math.pow(a2, 2.0) + Math.pow(b2, 2.0))
        val Cbarab = (C1ab + C2ab) / 2
        val G = 0.5 * (1 - Math.sqrt(Math.pow(Cbarab, 7.0) / (Math.pow(Cbarab, 7.0) + Math.pow(25.0, 7.0))))
        val aprime1 = (1 + G) * a1
        val aprime2 = (1 + G) * a2
        val Cprime1 = Math.sqrt(Math.pow(aprime1, 2.0) + Math.pow(b1, 2.0))
        val Cprime2 = Math.sqrt(Math.pow(aprime2, 2.0) + Math.pow(b2, 2.0))
        var hprime1: Double
        var hprime2: Double
        hprime1 = if (b1 == aprime1 && aprime1 == 0.0) {
            0.0
        } else {
            Math.toDegrees(Math.atan2(b1, aprime1))
        }
        hprime2 = if (b2 == aprime1 && aprime2 == 0.0) {
            0.0
        } else {
            Math.toDegrees(Math.atan2(b2, aprime2))
        }

        if (hprime1 < 0) {
            hprime1 += 360.0
        }
        if (hprime2 < 0) {
            hprime2 += 360.0
        }

        val deltaLprime = L2 - L1
        val deltaCprime = Cprime2 - Cprime1
        var deltahprime = 0.0
        when {
            Cprime1 * Cprime2 == 0.0 -> deltahprime = 0.0
            hprime2 - hprime1 <= 180 -> deltahprime = hprime2 - hprime1
            hprime2 - hprime1 > 180 -> deltahprime = hprime2 - hprime1 - 360.0
            hprime2 - hprime1 < -180 -> deltahprime = hprime2 - hprime1 + 360
        }
        val deltaHprime = 2.0 * Math.sqrt(Cprime1 * Cprime2) * Math.sin(Math.toRadians(deltahprime / 2))

        val Lprimebar = (L1 + L2) / 2
        val Cprimebar = (Cprime1 + Cprime2) / 2
        var hprimebar = 0.0
        if (hprime1 - hprime2 <= 180 && Cprime1 * Cprime2 != 0.0) {
            hprimebar = (hprime1 + hprime2) / 2
        }
        if (hprime1 - hprime2 > 180 && hprime1 + hprime2 < 360 && Cprime1 * Cprime2 != 0.0) {
            hprimebar = (hprime1 + hprime2 + 360.0) / 2
        }
        if (hprime1 - hprime2 > 180 && hprime1 + hprime2 >= 360 && Cprime1 * Cprime2 != 0.0) {
            hprimebar = (hprime1 + hprime2 - 360) / 2
        }
        if (Cprime1 * Cprime2 == 0.0) {
            hprimebar = hprime1 + hprime2
        }
        val T = 1 - 0.17 * Math.cos(Math.toRadians(hprimebar - 30)) + 0.24 * Math.cos(Math.toRadians(2 * hprimebar)) + 0.32 * Math.cos(Math.toRadians(3 * hprimebar + 6)) - 0.2 * Math.cos(Math.toRadians(4 * hprimebar - 63))
        val deltatheta = 30 * Math.exp(-Math.pow((hprimebar - 275) / 25, 2.0))
        val Rc = 2 * Math.sqrt(Math.pow(Cprimebar, 7.0) / (Math.pow(Cprimebar, 7.0) + Math.pow(25.0, 7.0)))
        val Sl = 1 + 0.015 * Math.pow(Lprimebar - 50, 2.0) / Math.sqrt(20 + Math.pow(Lprimebar - 50, 2.0))
        val Sc = 1 + 0.045 * Cprimebar
        val Sh = 1 + 0.015 * Cprimebar * T
        val Rt = -Math.sin(Math.toRadians(2 * deltatheta)) * Rc
        return Math.sqrt(Math.pow(deltaLprime / (parametric * Sl), 2.0) + Math.pow(deltaCprime / (parametric * Sc), 2.0) + Math.pow(deltaHprime / (parametric * Sh), 2.0) + Rt * (deltaCprime / (parametric * Sc)) * (deltaHprime / (parametric * Sh)))
    }

}
