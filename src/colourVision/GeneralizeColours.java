package colourVision;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class GeneralizeColours {

    private static HashMap<Color, Integer> colorMap;
    private static Color[] processColours;
    private static ArrayList<HashMap<Integer,Color>> similarColours;

    public static Color[] process(ArrayList<Color> colours) {
        colorMap = new HashMap<>();
        similarColours = new ArrayList<>();
        processColours = new Color[colours.size()];
        groupSimilarColours(colours);
        getSimilarColoursColour(similarColours);
        return processColours;
    }

    private static void groupSimilarColours(ArrayList<Color> colours) {
        int index=0;
        for (Color colour : colours) {
            int indexOfArrayList = getIndexOfClosestColour(colour);
            if(indexOfArrayList == -1) {
                HashMap<Integer,Color> newMap = new HashMap<>();
                newMap.put(index,colour);
                similarColours.add(newMap);
                colorMap.put(colour,similarColours.indexOf(newMap));
            } else {
                similarColours.get(indexOfArrayList).put(index,colour);
                colorMap.put(colour,indexOfArrayList);
            }
            index++;
        }
    }

    private static void getSimilarColoursColour(ArrayList<HashMap<Integer, Color>> similarColours) {
        for (HashMap<Integer, Color> colourSet : similarColours) {
            int rowRed = 0;
            int rowGreen = 0;
            int rowBlue = 0;
            for (Color color : colourSet.values()) {
                rowRed+=color.getRed();
                rowGreen+=color.getGreen();
                rowBlue+=color.getBlue();
            }
            int newRed = rowRed/colourSet.size();
            int newGreen = rowGreen/colourSet.size();
            int newBlue = rowBlue/colourSet.size();
            Color rowColour = new Color(newRed,newGreen,newBlue);
            for (Integer key : colourSet.keySet()) {
                processColours[key] = rowColour;
            }
        }
    }

    private static int getIndexOfClosestColour(Color colour) {
        if(colorMap.size() == 0) {
            return -1;
        }
        int closestKey = -1;
        double closestDistance = -1;
        for (Color colourInMap : colorMap.keySet()) {
            double[] colourLAB = Distance.INSTANCE.rgb2lab(colour);
            double[] colourInMapLAB = Distance.INSTANCE.rgb2lab(colourInMap);
            double colourDistance = Distance.INSTANCE.getDistance(colourLAB,colourInMapLAB);
            if(colourDistance<closestDistance || closestDistance==-1) {
                if(colourDistance<=10) {
                    closestKey = colorMap.get(colourInMap);
                    closestDistance = colourDistance;
                }
            }
        }
        return closestKey;
    }

    public static float[][] toFloat(Color[] colours) {
        float[][] outerArray = new float[colours.length][3];
        for (int i = 0; i <colours.length; i++) {
            float[] hsv = Color.RGBtoHSB(colours[i].getRed(),colours[i].getGreen(),colours[i].getBlue(),null);
            outerArray[i]=hsv;
        }
        return outerArray;
    }

}
