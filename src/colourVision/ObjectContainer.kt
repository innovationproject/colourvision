package colourVision

import java.awt.Color
import java.awt.geom.Point2D

class ObjectContainer {

    private var red = 0
    private var green = 0
    private var blue = 0
    private var count = 0
    private var totalX = 0
    private var totalY = 0
    private var maxY = 0
    private var minY = 0
    private var maxX = 0
    private var minX = 0

    private fun addColour(colourToAdd: Color) {
        red+=colourToAdd.red
        green+=colourToAdd.green
        blue+=colourToAdd.blue
        count++
    }

    fun getColour():Color {
        return Color(red / count, green / count, blue / count)
    }

    fun addPoint(pointToAdd: Point2D.Double, pointColour: Color) {
        totalY+=pointToAdd.y.toInt()
        totalX+=pointToAdd.x.toInt()
        if(maxY<pointToAdd.y) {
            maxY = pointToAdd.y.toInt()
        }
        if(minY == 0 || minY > pointToAdd.y) {
            minY = pointToAdd.getY().toInt()
        }
        if(maxX < pointToAdd.x) {
            maxX = pointToAdd.x.toInt()
        }
        if(minX == 0 || minX > pointToAdd.x) {
            minX = pointToAdd.x.toInt()
        }
        addColour(pointColour)
    }

    fun getCenterOfMassY():Int {
        return totalY/count
    }

    fun getCenterOfMassX():Int {
        return totalX/count
    }

    fun getSquareArea():Int {
        return ((maxY-minY)+1)*((maxX-minX)+1)
    }

    fun getMaxY():Int {
        return maxY
    }

    fun getMinY():Int {
        return minY
    }

    fun getMaxX():Int {
        return maxX
    }

    fun getMinX():Int {
        return minX
    }
}