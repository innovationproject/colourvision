package testPackage

import colourVision.AverageColour
import colourVision.GeneralizeColours

fun main(args: Array<String>) {
    val imagePath = "badOne.jpg"
    val backgroundPath = "paper.jpg"
    val tester = AverageColour()
    tester.setBackgroundColour(backgroundPath)
    val colours = tester.getColoursEncoded(imagePath)
    val floatColours = GeneralizeColours.toFloat(colours)
    for (floatColour in floatColours) {
        println("this is a float colour")
    }
}